ocaml-inotify (2.6-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 4.7.0

 -- Stéphane Glondu <glondu@debian.org>  Tue, 20 Aug 2024 08:49:12 +0200

ocaml-inotify (2.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Sun, 18 Feb 2024 11:52:54 +0100

ocaml-inotify (2.4.1-5) unstable; urgency=medium

  * Team upload
  * Use ocaml_dune DH buildsystem

 -- Stéphane Glondu <glondu@debian.org>  Thu, 17 Aug 2023 07:58:18 +0200

ocaml-inotify (2.4.1-4) unstable; urgency=medium

  * Team upload.
  * Bump standards-version to 4.6.2.
  * Fix compilation with recent dune

 -- Julien Puydt <jpuydt@debian.org>  Thu, 06 Jul 2023 11:21:42 +0200

ocaml-inotify (2.4.1-3) unstable; urgency=medium

  * Team upload
  * Fix FTBFS on bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Sat, 04 Feb 2023 11:58:21 +0100

ocaml-inotify (2.4.1-2) unstable; urgency=medium

  * Team upload
  * Add again ocaml-findlib to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Sat, 04 Feb 2023 08:31:19 +0100

ocaml-inotify (2.4.1-1) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * New upstream release

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Update watch file format version to 4.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Update pattern for GitHub archive URLs from /<org>/<repo>/tags
    page/<org>/<repo>/archive/<tag> -> /<org>/<repo>/archive/refs/tags/<tag>.
  * debian/watch: Use GitHub /tags rather than /releases page.
  * Update standards version to 4.6.1, no changes needed.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 04 Feb 2023 07:29:56 +0100

ocaml-inotify (2.3-2) unstable; urgency=medium

  * Export CAML_LD_LIBRARY_PATH in debian/rules so that tests can run on
    bytecode architectures

 -- Stéphane Glondu <glondu@debian.org>  Mon, 24 Aug 2020 11:44:22 +0200

ocaml-inotify (2.3-1) unstable; urgency=medium

  * Team upload
  * Update Homepage and debian/watch
  * New upstream release
  * Bump debhelper compat level to 13 and stop using cdbs
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Mon, 24 Aug 2020 07:56:07 +0200

ocaml-inotify (1.0-3) unstable; urgency=medium

  * Team upload
  * Update Vcs-*
  * Fix compilation with OCaml 4.08.0

 -- Stéphane Glondu <glondu@debian.org>  Thu, 05 Sep 2019 09:05:32 +0200

ocaml-inotify (1.0-2) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Set Architecture to linux-any
  * Bump debhelper compat to 10
  * Switch source package format to 3.0 (quilt)
  * Update Vcs-*

  [ Sylvain Le Gall ]
  * Remove myself from uploaders (Closes: #869316)

  [ Hendrik Tews ]
  * fix watch file

 -- Stéphane Glondu <glondu@debian.org>  Thu, 27 Jul 2017 05:52:39 +0200

ocaml-inotify (1.0-1) unstable; urgency=low

  * New upstream release
  * Switch to dh-ocaml 0.9.1 to compute dependencies
  * Upgrade Standards-Version to 3.8.3 (no change)
  * Add debian/libinotify-ocaml-dev.ocamldoc to generate ocamldoc
    documentation

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 25 Oct 2009 17:25:35 +0000

ocaml-inotify (0.9-1) unstable; urgency=low

  * Add debian/gbp.conf to force using pristine-tar
  * New Upstream Version (Closes: #521870)
  * Change section to "ocaml"
  * Upgrade Standards-Version to 3.8.1 (no change)

 -- Sylvain Le Gall <gildor@debian.org>  Wed, 15 Apr 2009 15:58:56 +0200

ocaml-inotify (0.7-2) unstable; urgency=low

  * Add dh-ocaml build-dependency (rules/ocaml.mk)
  * Set maintainer to Debian OCaml Maintainers
  * Upgrade Standards-Version to 3.8.0 (debian/README.source)
  * Add ${misc:Depends} to dependencies
  * Update debian/copyright file to use
    http://wiki.debian.org/Proposals/CopyrightFormat

 -- Sylvain Le Gall <gildor@debian.org>  Sat, 07 Mar 2009 14:58:03 +0100

ocaml-inotify (0.7-1) unstable; urgency=low

  * New Upstream Version:
    * Remove patch 01_nogcc, 02_META and 03_ocamlopt which have been applied
      upstream
    * Prevent removal of ocaml-sqlite3 in the clean process

 -- Sylvain Le Gall <gildor@debian.org>  Mon, 28 Jul 2008 22:32:34 +0200

ocaml-inotify (0.6-1) unstable; urgency=low

  * Initial release. (Closes: #485134)

 -- Sylvain Le Gall <gildor@debian.org>  Sun, 08 Jun 2008 11:46:51 +0200
